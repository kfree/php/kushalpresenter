<?php

/**
 * This does whatever it wants to.
 */

namespace Drupal\subtonode\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\node\Entity\Node;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\Core\Messenger\MessengerTrait;

class SubToNodeController extends ControllerBase {
  use MessengerTrait;

  public function subtonode($webform_submission) {
    //$sid = 2;
    // example https://kushalpresenter.loc/admin/structure/webform/manage/kushal-song-of-the-day/submission/1/edit/subtonode
    $node_details = WebformSubmission::load($webform_submission);

    if ($node_details != NULL) {
      $submission_array = $node_details->getOriginalData();
      $field_acknowledgement = $submission_array['i_affirm_that_this_is_my_song_of_the_day'];
      $field_song_title = $submission_array['what_is_your_song_of_the_day_'];

      // Create node object with attached file.
      $node = Node::create([
        'type' => 'song_of_the_day',
        'title' => "Song of the day",
        'field_acknowledgement' => $field_acknowledgement,
        'field_song_title' => $field_song_title,
      ]);

      try {
        $node->save();
      } catch (EntityStorageException $e) {
        die();
      }

      $this->messenger()->addMessage(t('You have successfully created a node from webform submission @sid', ['@sid' => $webform_submission]));
      return array();
    }
    $this->messenger()->addError('We failed to create a node. I am very sorry.', False);
    return array();
  }

}

