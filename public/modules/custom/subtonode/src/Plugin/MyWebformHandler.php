<?php

namespace Drupal\mymodule\Plugin\WebformHandler;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\webform\WebformInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\webformSubmissionInterface;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Create a new Article node from a webform submission.
 *
 * @WebformHandler(
 *   id = "article_from_webform",
 *   label = @Translation("Create a node on submit"),
 *   category = @Translation("Content"),
 *   description = @Translation("Creates a new Article node from Webform Submissions."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */

class MyWebformHandler extends WebformHandlerBase {
  use MessengerTrait;

  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $node_details = WebformSubmission::load($webform_submission);

    if ($node_details != NULL) {
      $submission_array = $node_details->getOriginalData();
      $field_acknowledgement = $submission_array['i_affirm_that_this_is_my_song_of_the_day'];
      $field_song_title = $submission_array['what_is_your_song_of_the_day_'];

      // Create node object with attached file.
      $node = Node::create([
        'type' => 'song_of_the_day',
        'title' => "Song of the day",
//        'field_acknowledgement' => $field_acknowledgement,
//        'field_song_title' => $field_song_title,
      ]);

      try {
        $node->save();
      } catch (EntityStorageException $e) {
        \Drupal::logger('sub to node')->notice($e);
        die();
      }

      $this->messenger()
        ->addMessage('You have successfully created a node from webform submission @sid', ['@sid' => $webform_submission]);
      return [];
    }
    $this->messenger()
      ->addError('We failed to create a node. I am very sorry.', FALSE);
    return [];
  }
}
